/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.model.config;

import ee.envir.estmodel.EstModel.DiffuseSource;
import ee.envir.estmodel.EstModel.Parameter;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.validation.constraints.NotNull;

public class PastureConfiguration extends DiffuseSource.Configuration {

    @JsonbCreator
    public PastureConfiguration(@NotNull Parameter parameter) {

        super(parameter);

        this.setDrainageFactor(0.3);
        this.setFertilizerFactor(0.7);
        this.setClaySoilFactor(0.1);
        this.setFertileSoilFactor(0.1);
        this.setPeatSoilFactor(0.05);
        this.setSandSoilFactor(0.05);

        switch (parameter) {

            case TN -> {
                this.setMaxConcentration(3.6);
                this.setMaxFertilizerAmount(134.0 * 100 * 3);
            }

            case TP -> {
                this.setMaxConcentration(0.07);
                this.setMaxFertilizerAmount(30.1 * 100 * 3);
            }

            default ->
                throw new IllegalArgumentException();

        }

    }

}
