/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.model;

import static java.lang.Double.isNaN;
import java.time.LocalDate;
import java.util.stream.Stream;

public interface RiverStation<S extends RiverStation<S>>
        extends RiverPoint, Basin {

    Stream<S> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate);

    default boolean isMonitoring(Object parameter, LocalDate startDate, LocalDate endDate) {

        return !isNaN(this.discharge(parameter, startDate, endDate));

    }

    default double separateDischarge(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.discharge(parameter, startDate, endDate)
                - this.findEnclosingStations(parameter, startDate, endDate)
                        .mapToDouble(s -> s.discharge(parameter, startDate, endDate))
                        .sum();

    }

    default double separateSpecificDischarge(Object parameter, LocalDate startDate, LocalDate endDate) {

        var stations = this.findEnclosingStations(parameter, startDate, endDate).toList();

        return (this.discharge(parameter, startDate, endDate) - stations.stream()
                .mapToDouble(s -> s.discharge(parameter, startDate, endDate)).sum())
                / (this.getCalculationArea() - stations.stream()
                .mapToDouble(RiverStation::getCalculationArea).sum());

    }

}
