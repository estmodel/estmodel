/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Stream;

public interface RiverBasinDistrictSubunit<R extends RiverBasin<R, S>, S extends RiverStation<S>>
        extends RiverBasinDistrict<S> {

    @Override
    default double discharge(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.specificDischarge(parameter, startDate, endDate) * this.getArea();

    }

    @Override
    default Stream<S> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getRivers().stream()
                .filter(r -> r.isDistrictRiver() && r.getParentRiver()
                .filter(pr -> this.equals(pr.getDistrict()) && pr.isDistrictRiver()).isEmpty())
                .flatMap(r -> r.findEnclosingStations(parameter, startDate, endDate));
    }

    Collection<R> getRivers();

    @Override
    default double specificDischarge(Object parameter, LocalDate startDate, LocalDate endDate) {

        var stations = this.findEnclosingStations(parameter, startDate, endDate).toList();

        double monitoredArea = stations.stream()
                .mapToDouble(RiverStation::getCalculationArea)
                .sum();

        double monitoredDischarge = stations.stream()
                .mapToDouble(s -> s.discharge(parameter, startDate, endDate))
                .sum();

        return monitoredDischarge / monitoredArea;

    }

}
