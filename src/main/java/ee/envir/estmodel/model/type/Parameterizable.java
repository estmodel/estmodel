/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.model.type;

import ee.envir.estmodel.EstModel.Parameter;
import jakarta.json.bind.annotation.JsonbProperty;
import java.io.Serializable;

public abstract class Parameterizable implements Serializable {

    private final Parameter parameter;

    protected Parameterizable(Parameter parameter) {
        this.parameter = parameter;
    }

    @JsonbProperty(nillable = true)
    public Parameter getParameter() {
        return this.parameter;
    }

}
